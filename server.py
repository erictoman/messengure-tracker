import socket
import json
import logging
import threading
import datetime
import copy
from time import sleep

INACTIVITY_TIME = 99999  # seg

"""
Resumen: Esta función recibe datos hasta que el otro extremo deje de enviar.
Parámetros:
    <socket> sock:      Socket del cual recibir datos.
    <int> buffer_size:  Qué tan grande es el búfer para recibir datos. 2^buffer_size (10-14 está bien)

Regresa:
    <bytes> data    : Datos recibidos del socket.

"""


def recvall(sock, buffer_size=None):
    if(buffer_size):
        buffer_size = 2**buffer_size
    else:
        buffer_size = 512
    data = bytes()
    while True:
        part = sock.recv(buffer_size)
        data += part
        if (len(part) < buffer_size):
            break
    return data


"""
Resumen: Esta clase se encarga de iniciar una nueva instancia de un servidor 
que se encarga de recibir y enviar datos al cliente.
Cada nodo en la tabla puede durar máximo 5 minutos antes de necesitar
conectarse otra vez. (arquitectura p2p)

Parámetros:   
    <int> socket_port:    Puerto en el cual se ejecutará el servidor.

"""


class Server():
    #
    # Inicializar el socket (TCP bloqueante con multihilo)
    #
    # Estructura de la lista de peers en este tracker:
    # [{"ip":1.2.3.4, ,"datos":DATOS.QUE.SEAN.DE.UTILIDAD, "expirationTime":s}]
    #
    # Comandos para el servidor:
    #
    # <json> "['getAndUpdate',{literalmente lo que sea}]": Regresa la lista de peers (sin expirationTime) y actualiza
    #                           el tiempo de expiración para dicha IP.
    #
    #
    # <json> "['killMyself']"  Elimina la IP que elabore la solicitud
    #          de la lista de peers.
    #
    #
    def __init__(self, socket_port):
        print("IP SERVER: "+(([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")] or [[(s.connect(
            ("8.8.8.8", 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) + ["no IP found"])[0])
        self.peerlist = []
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try:
            self.socket.bind(("0.0.0.0", socket_port))
        except Exception as e:
            logging.error(str(e))
        self.socket.listen(socket_port)

        threading.Thread(target=self.__peer_list_updater).start()

        while True:
            client, address = self.socket.accept()
            logging.info(" Got connection from: "+str(client.getpeername()))
            threading.Thread(target=self.__listen_to_client,
                             args=(client, address)).start()  # thread

    def __peer_list_updater(self):
        while True:
            logging.info(" There are "+str(len(self.peerlist))+" peer(s).")
            date_now = datetime.datetime.now()
            for peer in self.peerlist:
                timedelta = date_now - peer["expTime"]
                if(timedelta.seconds > INACTIVITY_TIME):
                    logging.info(" REMOVING AFTER INACTIVITY: "+str(peer))
                    self.peerlist.remove(peer)
            sleep(INACTIVITY_TIME)

    def __send_list_of_peers(self):
        modifiedlist = copy.deepcopy(self.peerlist)
        [entry.pop("expTime", None) for entry in modifiedlist]
        modifiedlist = json.dumps(modifiedlist)
        return modifiedlist.encode()

    # Resumen: Busca el valor de una llave en un arreglo de diccionarios en una lista.

    def __search_in_list(self, theList, ip, field):
        return next((item for item in theList if item[field] == ip), None)

    def __listen_to_client(self, client, address):
        while True:
            try:
                data = recvall(client)
                if data:
                    client_ip = client.getpeername()[0]
                    data = json.loads(data)
                    current_entry = self.__search_in_list(
                        self.peerlist, client_ip, "ip")
                    if data[0] == "getAndUpdate":
                        if current_entry is not None:
                            self.peerlist.remove(current_entry)
                        self.peerlist.append({
                            "ip":       client_ip,
                            "data":     data[1],
                            "expTime":  datetime.datetime.now()
                        })
                        client.send(self.__send_list_of_peers())
                    if data[0] == "killMyself" and current_entry is not None:
                        logging.info(" Removed on request: " +
                                     str(client.getpeername()))
                        self.peerlist.remove(current_entry)
                else:
                    raise ValueError("Client disconnected")
            except Exception as e:
                client.close()
                break


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    Server(1234)
    print('aaa')
