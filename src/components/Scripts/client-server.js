const net = require("net");
const easysocket = require("./LIBS/easysocket.js");
export default class ClientServer {
  constructor(port, name) {
    this.port = port;
    this.sock;
    this.name = name;
  }
  initServer() {
    this.sock = net.createServer();
    this.sock.listen(this.port);
  }
  initCli() {
    this.sock = new net.Socket();
  }
  Envia(socket, obj) {
    easysocket.send(socket, JSON.stringify(obj), function(sock) {
      console.log("Enviando informacion");
    });
  }
  parser(data) {
    return JSON.parse(data);
  }
}

function handleMessage(socket, buffer) {
  var obj = JSON.parse(buffer.toString());
  console.log(obj.name, ":", obj.mensaje);
}

/*
//Server
console.log("Servidor inicializado")
this.sock.on("connection", (sock) => {
   sock.on("error", function (error) {
      //Desconectado
   })
   sock.on("close", function (error) {
      //Desconectado
   })
   sock.on('data', function (data) {
      //Datos recibidos
      easysocket.recieve(sock, data, handleMessage);
   });
});
function handleMessage(socket, buffer) {
   var obj = JSON.parse(buffer.toString())
   console.log(obj.name, ":", obj.mensaje)
}
*/
