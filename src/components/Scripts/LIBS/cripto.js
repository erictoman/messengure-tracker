var keypair = require('keypair')
var NodeRSA = require('node-rsa')
var fs = require('fs')
var genKeys = function (path) {
    return new Promise((resolve) => {
        var pair = keypair({
            bits: 1024
        });
        var dt = new Date();
        var date = dt.getFullYear() + "-" + (dt.getMonth() + 1) + "-" + dt.getDate();
        fs.writeFile(path + "/Public_key_" + date + ".pem", Buffer.from(pair.public), () => {
            fs.writeFile(path + "/Private_key_" + date + ".pem", Buffer.from(pair.private), () => {
                resolve(1)
            })
        })
    })
}
var RSACipher = function (text, keypath, opt) {
    var keyRSA = fs.readFileSync(keypath).toString()
    if (opt) {
        const key = new NodeRSA(keyRSA, "pkcs1-public")
        return key.encrypt(text, 'base64');
    } else {
        const key = new NodeRSA(keyRSA, "pkcs1-private")
        return key.decrypt(text, 'utf8');
    }
}

module.exports = {
    genKeys,
    RSACipher
}