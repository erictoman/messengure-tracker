import Vue from 'vue'
import App from './App.vue'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import Autoscroll from 'vue-autoscroll'
var tools = require("./components/Scripts/LIBS/cripto")
Vue.use(tools)
Vue.use(Autoscroll)
Vue.use(Buefy)
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')